rule fastqc1:
	input: lambda wildcards: samples.at[wildcards.sample,'fq1'] if wildcards.sample in samples.index else ""
	output:
		html="results/fastqc1/{sample}_1.html",
		zip= "results/fastqc1/{sample}_1_fastqc.zip",
	params: ""
	threads: 4
	conda:
		"../envs/env_qc.yaml"
	wrapper:
		"0.35.1/bio/fastqc"

rule fastqc2:
	input: lambda wildcards: samples.at[wildcards.sample,'fq2'] if wildcards.sample in samples.index else ""
	output:
		html= "results/fastqc1/{sample}_2.html",
		zip = "results/fastqc1/{sample}_2_fastqc.zip",
	params:""
	threads: 4
	conda:
		"../envs/env_qc.yaml"
	wrapper:
		"0.35.1/bio/fastqc"


rule multiqc:
	input:
		expand("results/fastqc1/{sample}_1.html", sample=list(samples.index)),
		expand("results/fastqc1/{sample}_2.html", sample=list(samples.index))
	output:
		"results/multiqc/multiqc.html"
	params: ""
	threads: 4
	conda:
		"../envs/env_qc.yaml"
	wrapper:
		"0.35.1/bio/multiqc"

rule fastqc1_trimm:
	input:
		r1 = "results/trimm/{sample}_1.fastq",
	output:
		html="results/fastqc2/{sample}_1.html",
		zip="results/fastqc2/{sample}_1_fastqc.zip"
	params: ""
	threads: 4
	conda:
		"../envs/env_qc.yaml"
	wrapper:
		"0.35.1/bio/fastqc"

rule fastqc2_trimm:
	input:
		r1 = "results/trimm/{sample}_2.fastq",
	output:
		html="results/fastqc2/{sample}_2.html",
		zip="results/fastqc2/{sample}_2_fastqc.zip"
	params: ""
	threads: 4
	conda:
		"../envs/env_qc.yaml"
	wrapper:
		"0.35.1/bio/fastqc"

rule multiqc_trimm:
	input:
		expand("results/fastqc2/{sample}_1.html", sample = list(samples.index)),
		expand("results/fastqc2/{sample}_2.html", sample = list(samples.index))
	output:
		"results/multiqc_trimm/multiqc.html"
	params: ""
	threads: 4
	conda:
		"../envs/env_qc.yaml"
	wrapper:
		"0.35.1/bio/multiqc"


rule quast:
	input:
		"results/assembly/{sample}/contigs.fasta"
	output:
		"results/quast/{sample}/report.html"
	threads: 4
	conda:
		"../envs/env_qc.yaml"
	shell:
		"quast {input} -o results/quast/{wildcards.sample}/"


rule multiqc_trimm_wquast:
	input:
		expand("results/fastqc2/{sample}_1.html", sample=list(samples.index)),
		expand("results/fastqc2/{sample}_2.html", sample=list(samples.index)),
		expand("results/quast/{sample}/report.html", sample=list(samples.index)),
	output:
		"results/multiqc_wquast/multiqc.html"
	params: ""
	log:
		"results/logs/multiqcwquast.log"
	threads: 4
	conda:
		"../envs/env_qc.yaml"
	wrapper:
		"0.35.1/bio/multiqc"

import os


def input():
    if os.listdir(path_toData)[0].rsplit('.')[1] == "fastq":
        return "results/assembly/{sample}/contigs.fasta"
    elif os.listdir(path_toData)[0].rsplit('.')[1] == "fasta":
        return config['data'] + "{sample}.fasta"


rule prokka:
    input:
        input()
    output:
        "results/prokka/{sample}/{sample}.gff"
    params:
        genus = config["prokka"]["genus"],
        kingdom = config["prokka"]["kingdom"],
    threads: 4
    conda:
        "../envs/env_prokka.yaml"
    shell:
        "prokka --cpus {threads} {input} --outdir results/prokka/{wildcards.sample}/ --prefix {wildcards.sample} --force --usegenus --genus {params.genus}  --kingdom {params.kingdom}"

rule denovo:
    input:
        r1 = "results/trimm/{sample}_1.fastq",
        r2 = "results/trimm/{sample}_2.fastq"
    output:
        "results/assembly/{sample}/contigs.fasta"
    params:
        spades = config["spades_params"]["careful"],
        abs_path = os.path.abspath(os.getcwd()),
    log:
        "results/log/{sample}.txt"
    threads: 4
    conda:
        "../envs/env_spades.yaml"
    shell:
        "spades.py -t {threads} --isolate -1 {input.r1} -2 {input.r2} -o results/assembly/{wildcards.sample}/ || true"

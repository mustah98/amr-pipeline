import os


def input():
    if os.listdir(path_toData)[0].rsplit('.')[1] == "fastq":
        return "results/assembly/{sample}/contigs.fasta"
    elif os.listdir(path_toData)[0].rsplit('.')[1] == "fasta":
        return config['data'] + "{sample}.fasta"



rule amrfinder:
    input:
        data = input()
    output:
        out = "results/amrfinder/"+"{sample}.tsv"
    params:
        amrfinde = config["amrfinder"]["organism"],
        id = config["amrfinder"]["id"],
        coverage = config["amrfinder"]["coverage"],

    threads: 4
    conda:
        "../envs/env_amrfinder.yaml"
    shell:
        "amrfinder --plus --threads {threads} --ident_min {params.id} --coverage_min {params.coverage} -n {input.data} -o {output.out}"
        # "{params.set_env} amrfinder --plus --threads {threads} -n {input.data} -O {params.amrfinde} -o {output.out}"
#

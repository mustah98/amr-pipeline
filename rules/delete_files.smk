import os
import shutil
import sys

rule delete:
    input:
        expand("results/assembly/{sample}/contigs.fasta", sample = list(samples.index)),
        # expnd("results/quast/{sample}/report.html", sample = list(samples.index)),
    output:
        "results/none2.txt"
    params:
        script =  os.path.abspath(os.getcwd()) + "/skripts/delete.py",
        path = os.path.abspath(os.getcwd()) + "/results/assembly/"
    run:
        def delete_files(path):
            print('deleting intermediate files')
            data = os.listdir(path)
            for elem in data:
                data_temp = os.listdir(path+elem)
                for file in data_temp:
                    a = path+elem+'/'+file
                    if os.path.isdir(a):
                        shutil.rmtree(a)
                    elif file == 'contigs.fasta' or file == 'scaffolds.fasta':
                        continue
                    else:
                        os.remove(a)
            print("fineshed deleting intermediate files")

        path_abs = os.path.abspath(os.getcwd())
        open(path_abs + "/results/none2.txt", "x")
        path = params.path
        delete_files(path)





















    # shell:
    #     "python {params.script} {input.path}"

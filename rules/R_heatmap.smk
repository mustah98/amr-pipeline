rule R_heatmap:
    input:
        'results/abricate_presence_absence.csv',
        'results/amrfinder_presence_absence.csv',
    output:
        'results/abricate_R_heatmap.png',
        'results/amrfinder_R_heatmap.png'
    params:
    conda:
        "envs/r.yaml"
    shell:
        "Rscript skripts/R_heatmap.r"

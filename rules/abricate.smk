import os


def input():
    if os.listdir(path_toData)[0].rsplit('.')[1] == "fastq":
        return "results/assembly/{sample}/contigs.fasta"
    elif os.listdir(path_toData)[0].rsplit('.')[1] == "fasta":
        return config['data'] + "{sample}.fasta"

rule abricate:
    input:
        data = input()
    output:
        out = "results/abricate/"+"{sample}.tsv"
    params:
        database = config["abricate"]["database"],
        minid = config["abricate"]["minid"],
        mincov = config["abricate"]["mincov"],
    threads: 4
    conda:
        "../envs/env_abricate.yaml"
    shell:
        "abricate --db {params.database} --threads {threads} --minid {params.minid} --mincov {params.mincov} {input.data} > {output.out}"

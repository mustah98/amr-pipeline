### This is the execution file to run the pipeline ###


import argparse
import os
import sys



parser = argparse.ArgumentParser()
parser.add_argument("-t", "--threads", help = "Amount of threads to use")
parser.add_argument("-p", "--path", help = "Path to data")
args = parser.parse_args()

if args.threads:
	print("Threads: % s" % args.threads)
	threads = args.threads
else:
	threads = 4
	print("Threads: % s" % threads)

if args.path:
	if os.path.exists(args.path):
		print("Path to data: % s" % args.path)
		path = args.path
	else:
		sys.exit("Error: Path to data-directory does not exist or was not provided!")


if sys.argv[1] != "--help" or sys.argv[1] != "-h":
    os.system("snakemake  --config data="+ path + " --use-conda --cores " + threads + " --conda-frontend mamba")

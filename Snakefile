import os
import sys
import importlib

pandas_loader = importlib.find_loader("pandas")
found = pandas_loader is not None
if found == False:
	os.system('conda install -c bioconda pandas')


seaborn_loader = importlib.find_loader("seaborn")
found = seaborn_loader is not None
if found == False:
	os.system('conda install -c bioconda seaborn')


matplotlib_loader = importlib.find_loader("matplotlib")
found = matplotlib_loader is not None
if found == False:
	os.system('conda install -c bioconda matplotlib')

pathlib_loader = importlib.find_loader("pathlib")
found = pathlib_loader is not None
if found == False:
	os.system('conda install -c bioconda pathlib')

numpy_loader = importlib.find_loader("numpy")
found = numpy_loader is not None
if found == False:
	os.system('conda install -c bioconda numpy')

import csv
import pandas as pd
import matplotlib.pyplot as plt

import numpy as np
import seaborn as sns
import shutil
configfile: "config.yaml"



path_abs = os.path.abspath(os.getcwd())

if os.path.isfile("results/none.txt"):
	os.remove("results/none.txt")
if os.path.isfile("results/none2.txt"):
	os.remove("results/none2.txt")

path_toData = config["data"]

def generate_sample_tsv(path_toData):
	files = sorted(os.listdir(path_toData))
	i=0
	j=0
	with open ("sample/sample.tsv", "w") as sample:
		if files[0].rsplit(".")[1] == "fastq" or files[0].rsplit(".")[1] == "zip" :
			sample.write("sample\tfq1\tfq2" + "\n")
			while j < (len(files) / 2):
				sample.write(files[i].split(".")[0]+ "\t")
				sample.write(path_toData + files[i] + "\t" + path_toData + files[i+1] + "\n")
				i = i + 2
				j = j + 1
			return
		elif files[0].rsplit(".")[1] == "fasta" or files[0].rsplit(".")[1] == "zip":
			sample.write("sample\tfq1\n")
			while i < len(files):
				sample.write(files[i].split(".")[0]+ "\t")
				sample.write(path_toData + files[i] + "\n")
				i += 1
			return

generate_sample_tsv(path_toData)


samples = pd.read_table(config["samples"], index_col = "sample")

DATASET = ["AB_genes_heatmap_amrfinder", "AB_genes_bar_amrfinder", "AB_genes_cluster_amrfinder", "AB_class_bar_amrfinder", "AB_heatmap_amrfinder"]
DATASET_ABRICATE = ["AB_genes_heatmap_abricate", "AB_genes_bar_abricate", "AB_genes_cluster_abricate", "AB_class_bar_abricate", "AB_class_heatmap_abricate"]
DATASET2 = ["heatmap_origin", "stacked_bar_origin", "heatmap_association", "stacked_bar_association", "heatmap_baps", "stacked_bar_baps"]

if os.path.isdir('results') == False:
    os.system("mkdir results")
os.system("touch results/none.txt")

def amr_tool_selection():
    if config["amr_tool"]["amr_finder_plus"] == True and config["amr_tool"]["abricate"] == False:
        return( expand("results/amrfinder/{sample}.tsv", sample = list(samples.index)),
                expand("results/amr_finder_plots/{Dataset}.png", Dataset = DATASET),
                "results/amrfinder_presence_absence.csv",
                )
    if config["amr_tool"]["abricate"] == True and config["amr_tool"]["amr_finder_plus"] ==  False:
        return( expand("results/abricate/{sample}.tsv", sample = list(samples.index)),
                expand("results/abricate_plots/{Dataset}.png", Dataset = DATASET_ABRICATE),
                "results/abricate_presence_absence.csv",
                )
    else:
        sys.exit("Error: Pleas provide one tool for AR Detection in the config file!")


def annotation():
	if config["annotation"] == True:
		return(expand("results/prokka/{sample}/{sample}.gff", sample = list(samples.index)))
	else:
		open("results/none.txt", "r")
		return 'results/none.txt'


def preTrimmControll():
	if config["QcOptions"]["beforTrimm"]  == True:
		return(
			expand("results/fastqc1/{sample}_1.html", sample=list(samples.index)), # QC ohne trimm 1
			expand("results/fastqc1/{sample}_2.html", sample=list(samples.index)), # QC ohne trimm 2
			"results/multiqc/multiqc.html"
		)
	else:
		open("results/none.txt", "r")
		return 'results/none.txt'

def postTrimmControll():
	if config["QcOptions"]["afterTrimm"]  == True:
		return(
			expand("results/fastqc2/{sample}_1.html", sample=list(samples.index)), # QC mit trimm 1
			expand("results/fastqc2/{sample}_2.html", sample=list(samples.index)), # QC mit trimm 2
			"results/multiqc_trimm/multiqc.html"
		)
	else:
		open("results/none.txt", "r")
		return 'results/none.txt'

def genomeControll():
	if config["QcOptions"]["genomeControll"]  == True:
		return(expand("results/quast/{sample}/report.html", sample = list(samples.index)))
	else:
		open("results/none.txt", "r")
		return 'results/none.txt'


def ruleAll():
	if os.listdir(path_toData)[0].split('.')[1] == "fastq":
		return(
			preTrimmControll(),
			expand("results/trimm/{sample}_1.fastq", sample = list(samples.index)),
			expand("results/trimm/{sample}_2.fastq", sample = list(samples.index)),
			postTrimmControll(),
			expand("results/assembly/{sample}/contigs.fasta", sample = list(samples.index)),
			genomeControll(),
			annotation(),
            amr_tool_selection()
			)
	elif os.listdir(path_toData)[0].split('.')[1] == "fasta":
		return(
			genomeControll(),
			annotation(),
            amr_tool_selection()
			)
rule all:
	input:
		ruleAll()




include: "rules/qc.smk"
include: "rules/trimm.smk"
include: "rules/assembly.smk"
include: "rules/amrfinder.smk"
include: "rules/abricate.smk"
include: "rules/results.smk"
include: "rules/annotation.smk"
include: "rules/delete_files.smk"
include: "rules/abricate_results.smk"
include: "rules/R_heatmap.smk"

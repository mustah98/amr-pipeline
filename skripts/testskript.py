import os
import csv
import pandas as pd
import matplotlib as mp
import matplotlib.pyplot as plt
import sys
import numpy as np
import seaborn as sns
from pathlib import Path
import warnings

if not sys.warnoptions:
    warnings.simplefilter("ignore")

def find_global_genes(data, path, global_gens):
    print("finding global genes..")

    i = 0
    for elem in data:
        file = open(path+elem)
        df = pd.read_csv(file, delimiter = "\t", encoding = "uft-8")
        for rows in df["Gene symbol"]:
            if rows not in global_gens:
                global_gens.append(rows)
        i = i + 1
        file.close()
    return(global_gens)


def find_gene(Matrix, data, path, global_gens):
    print("generate gene presence absence Matrix..")
    pos_strain = 0
    for i in range(len(data)):
        file = open(path+data[i])
        df = pd.read_csv(file, delimiter = "\t", encoding = "uft-8")
        for gene in df["Gene symbol"]:
            index = global_gens.index(gene)
            Matrix[i][index] = 1
        file.close()
    return Matrix


def get_num_of_gen(w,h, gen_counter, Matrix):
    print("generating gene count Matrix..")
    for i in range(w):
        counter = 0
        for j in range(h):
            if Matrix[j][i] == 1:
                counter += 1
            else:
                continue
        gen_counter.append(counter)
    return gen_counter

def plots(Matrix, gen_counter):

    print("plotting Heatmap..")
    abs_path = os.path.abspath(os.getcwd())
    plt.subplots(figsize=(20,30))
    sns.heatmap(Matrix, cmap="viridis",  cbar = False )
    plt.savefig(abs_path + "/../results/plots/plot_amrfinder2.png", dpi=500)
    plt.close()

    print("plotting Bar Plot..")
    y_pos = np.arange(len(global_gens))
    plt.bar(y_pos, gen_counter, align = 'center', alpha = 0.5)
    plt.xticks(y_pos, global_gens, rotation = 'vertical', size=5)
    plt.savefig(abs_path + '/../results/plots/bar_amrfinder_2.png',dpi=400)
    plt.close()

    print("plotting Clustered Heatmap..")
    sns.clustermap(Matrix, cbar = False, cmap = "viridis", figsize= (20,30))
    plt.savefig(abs_path + "/../results/plots/cluster_abricate_2.png", dpi = 500)
    plt.close()


def antibiotic_cluster (data, path):
    print("plotting AB Cluster")

    data_normalizer = mp.colors.Normalize()
    color_map = mp.colors.LinearSegmentedColormap(
    "my_map",
        {
            "red": [(0, 1.0, 1.0),
                    (1.0, .5, .5)],
            "green": [(0, 0.5, 0.5),
                    (1.0, 0, 0)],
            "blue": [(0, 0.50, 0.5),
                    (1.0, 0, 0)]
        }
    )
    # hardcodedt groups
    tetracycline_gens = ["tet(O)"]
    aminoglycoside_gens = ["aad9", "aadE", "aph(3')-IIIa", "ant(6)-Ia"]
    beta_lactam_gens = ["blaOXA-184", "blaOXA-193", "blaOXA-61", "blaOXA-465", "blaOXA-460",  "blaOXA-447", "blaOXA-589", "blaOXA", "blaOXA-451", "blaOXA-461", "blaOXA-625", "blaOXA-597", "blaOXA-611", "blaOXA-489", "blaTEM",  "blaOXA-630", "blaOXA-578", "blaOXA-578", "blaOXA-632"]
    macrolide_gens = ["50S_L22_A103V", "23S_A2075G", "emrE"]
    fluroquinolone_gens = ["gyrA_T86I"]
    other_gens = []

    names = ['Tetracycline', 'Aminoglycoside', 'Beta_lactam', 'Macrolide', 'Fluroquinolone', 'Others']
    antibiotic_counter = []
    tetracycline = 0
    aminoglycoside = 0
    beta_lactam = 0
    macrolide = 0
    fluroquinolone = 0
    others = 0

    for elem in data:
        file = open(path+elem)
        df = pd.read_csv(file, delimiter = "\t", encoding = "uft-8")
        for gene in df["Gene symbol"]:
            if gene in tetracycline_gens:
                tetracycline += 1
            elif gene in aminoglycoside_gens:
                aminoglycoside += 1
            elif gene in beta_lactam_gens:
                beta_lactam += 1
            elif gene in macrolide_gens:
                macrolide += 1
            elif gene in fluroquinolone_gens:
                fluroquinolone += 1
            else:
                other_gens.append(gene)
                others += 1
    antibiotic_counter.append(tetracycline)
    antibiotic_counter.append(aminoglycoside)
    antibiotic_counter.append(beta_lactam)
    antibiotic_counter.append(macrolide)
    antibiotic_counter.append(fluroquinolone)
    antibiotic_counter.append(others)


    plt.figure(figsize = (12,12))
    plt.bar(names, antibiotic_counter, align = 'center', alpha = 0.5, color=color_map(data_normalizer(antibiotic_counter)))
    plt.xticks(rotation = 45, size=9)
    plt.legend(labels = ['Counts of Genes'])
    plt.xlabel('Antibiotics')
    plt.ylabel('Total Genes')
    plt.savefig(os.path.abspath(os.getcwd()) + "/../results/plots/AB_cluster.png", dpi = 500)



if __name__ == '__main__':


    Path(os.path.abspath(os.getcwd()) + "/../results/plots/").mkdir(parents = True, exist_ok = True)
    pd.set_option('display.max_columns', 50)
    # pd.set_option('display.max_rows', 350)
    path = os.path.abspath(os.getcwd()) + "/../results/amrfinder/"
    data = sorted(os.listdir(path))
    global_gens = []
    gen_counter = []
    name = [file.split('.')[0] for file in data]
    find_global_genes(data, path, global_gens)
    w= len(global_gens)
    h= len(data)
    Matrix = [[0 for i in range(w)] for j in range(h)]
    find_gene(Matrix, data, path, global_gens)
    get_num_of_gen(w, h, gen_counter, Matrix)
    Matrix = pd.DataFrame(Matrix, columns = global_gens, index = name)
    plots(Matrix, gen_counter)
    antibiotic_cluster(data, path)  #Hardcode - not sutiable for Pipeline
    print("FINESHED")

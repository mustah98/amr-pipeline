import os
import sys
import importlib

pandas_loader = importlib.find_loader("pandas")
found = pandas_loader is not None
if found == False:
    os.system('conda install -c bioconda pandas')


seaborn_loader = importlib.find_loader("seaborn")
found = seaborn_loader is not None
if found == False:
    os.system('conda install -c bioconda seaborn')


matplotlib_loader = importlib.find_loader("matplotlib")
found = matplotlib_loader is not None
if found == False:
    os.system('conda install -c bioconda matplotlib')

pathlib_loader = importlib.find_loader("pathlib")
found = pathlib_loader is not None
if found == False:
    os.system('conda install -c bioconda pathlib')

numpy_loader = importlib.find_loader("numpy")
found = numpy_loader is not None
if found == False:
    os.system('conda install -c bioconda numpy')


import pandas as pd
import matplotlib.pyplot as plt
from matplotlib import cm
import numpy as np
import seaborn as sns
from pathlib import Path


print("using the NCBI database script...")
##################
# imports
#################

abs_path = os.path.abspath(os.getcwd())
files = os.listdir(abs_path+"/results/abricate")
names = [file.split(".")[0] for file in files]


##################
# Antibiotic classes identifier
#################

def AB_class_ncbi(files, abs_path):
	AB_classes = []
	for file in files:
		df = pd.read_csv(abs_path+"/results/abricate/"+file, sep = "\t")
		for resistance in df["RESISTANCE"]:
			class_ = resistance
			if class_ not in AB_classes:
				AB_classes.append(class_)
	return AB_classes

def AB_class_count_ncbi(files, AB_classes, abs_path):
	AB_class_count_matrix = []
	for file in files:
		AB_class_count = [0 for elem in AB_classes]
		df = pd.read_csv(abs_path+"/results/abricate/"+file, sep = "\t")
		for resistance in df["RESISTANCE"]:
			class_ = resistance
			index = AB_classes.index(class_)
			AB_class_count[index] += 1
		AB_class_count_matrix.append(AB_class_count)
	return AB_class_count_matrix



##################
# Antibiotic genes identifier
#################

def AB_genes_ncbi(files, abs_path):
	AB_genes = []
	for file in files:
		df = pd.read_csv(abs_path+"/results/abricate/"+file, sep = "\t")
		for gene in df["GENE"]:
			if gene not in AB_genes:
				AB_genes.append(gene)
	return AB_genes

def AB_genes_count_ncbi(files, AB_genes, abs_path):
	AB_genes_count_matrix = []
	for file in files:
		AB_genes_count = [0 for elem in AB_genes]
		df = pd.read_csv(abs_path+"/results/abricate/"+file, sep = "\t")
		for gene in df["GENE"]:
			index = AB_genes.index(gene)
			AB_genes_count[index] += 1
		AB_genes_count_matrix.append(AB_genes_count)
	return AB_genes_count_matrix


##################
# AB class plots
#################


def colors_from_values(values, palette_name):
	normalized = (values - min(values)) / (max(values) - min(values))
	indices = np.round(normalized * (len(values) - 1)).astype(np.int32)
	palette = sns.color_palette(palette_name, len(values))
	return np.array(palette).take(indices, axis=0)


def make_plots_AB_classes(AB_classes, AB_class_count_matrix, names, abs_path):

	sns.set(style="whitegrid", color_codes=True)
	df = pd.DataFrame(AB_class_count_matrix)
	df.columns = AB_classes
	df.index = names
	sum_of_AB_classes = df.sum(axis = 0).tolist()
	df.to_csv(abs_path+"/results/abricate_AB_class_counts.csv", sep = ",", index = False)


	data = {"class" : AB_classes,
			"values" : sum_of_AB_classes,
			}

	data = pd.DataFrame(data, columns = ["class", "values"])


	plt.subplots(figsize=(10,10))
	sns.heatmap(df, cmap="viridis",  cbar = True )
	plt.savefig(abs_path + "/results/abricate_plots/AB_class_heatmap_abricate.png", dpi=300, bbox_inches="tight")
	plt.close()

	plt.subplots(figsize=(12,12))
	plots = sns.barplot(x = AB_classes, y = sum_of_AB_classes, palette = colors_from_values(np.array(sum_of_AB_classes), "YlOrRd"))
	for bar in plots.patches:
		plots.annotate(format(bar.get_height(), '.2f'),
		(bar.get_x() + bar.get_width() / 2,
		bar.get_height()), ha='center', va='center',
		size=15, xytext=(0, 5),
		textcoords='offset points')
	plots.set_xticklabels(plots.get_xticklabels(), rotation=90, horizontalalignment='right')
	plt.savefig(abs_path + "/results/abricate_plots/AB_class_bar_abricate.png", dpi=300, bbox_inches="tight")
	plt.close()

##################
# AB genes plots
#################






def make_plots_AB_genes(AB_genes, AB_genes_count_matrix, names, abs_path):
	sns.set(style="whitegrid", color_codes=True)

	df = pd.DataFrame(AB_genes_count_matrix)
	df.columns = AB_genes
	df.index = names
	df.to_csv(abs_path + "/results/abricate_presence_absence.csv", sep = ",", index = False)

	sum_of_AB_genes = df.sum(axis = 0)

	data = {"genes" : AB_genes,
			"vlaues" : sum_of_AB_genes,
			}
	data = pd.DataFrame(data, columns = ["genes", "values"])


	plt.subplots(figsize=(10,10))
	sns.heatmap(df, cmap="viridis",  cbar = True )
	plt.savefig(abs_path + "/results/abricate_plots/AB_genes_heatmap_abricate.png", dpi=300, bbox_inches="tight")
	plt.close()


	plt.figure(figsize = (12,12))
	plots = sns.barplot(x = AB_genes, y = sum_of_AB_genes, palette=colors_from_values(np.array(sum_of_AB_genes), "YlOrRd"))
	for bar in plots.patches:
		plots.annotate(format(bar.get_height(), '.2f'),
		(bar.get_x() + bar.get_width() / 2,
		bar.get_height()), ha='center', va='center',
		size=15, xytext=(0, 5),
		textcoords='offset points')
	plots.set_xticklabels(plots.get_xticklabels(), rotation=90, horizontalalignment='right')
	plt.savefig(abs_path + '/results/abricate_plots/AB_genes_bar_abricate.png', dpi=300, bbox_inches="tight")
	plt.close()

	sns.clustermap(df, cbar = False, cmap = "viridis", figsize= (20,30))
	plt.savefig(abs_path + "/results/abricate_plots/AB_genes_cluster_abricate.png",  dpi=300, bbox_inches="tight")
	plt.close()


AB_classes = AB_class_ncbi(files, abs_path)
AB_class_count_matrix = AB_class_count_ncbi(files, AB_classes, abs_path)

AB_genes = AB_genes_ncbi(files, abs_path)
AB_genes_count_matrix = AB_genes_count_ncbi(files, AB_genes, abs_path)

make_plots_AB_classes(AB_classes, AB_class_count_matrix, names, abs_path)
make_plots_AB_genes(AB_genes, AB_genes_count_matrix, names, abs_path)
